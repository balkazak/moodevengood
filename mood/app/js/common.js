$(function() {

	AOS.init();

	var speed = 'slow';

	$('html, body').hide();

	$(document).ready(function() {
		$('html, body').fadeIn(speed, function() {
			$('a[href], button[href]').click(function(event) {
				var url = $(this).attr('href');
				if (url.indexOf('#') == 0 || url.indexOf('javascript:') == 0) return;
				event.preventDefault();
				$('html, body').fadeOut(speed, function() {
					window.location = url;
				});
			});
		});
	});


	var galleryThumbs = new Swiper('.gallery-thumbs', {
		spaceBetween: 10,
		slidesPerView: 4,
		freeMode: true,
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
	});
	var galleryTop = new Swiper('.gallery-top', {
		spaceBetween: 10,
		slidesPerView: 'auto',
		effect: 'coverflow',

		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		thumbs: {
			swiper: galleryThumbs
		}
	});

	$('.top-line-center span').on('click', function()  {
		$('.select-form').slideToggle('400');
		$('.top-line-center span .fa').toggleClass('arrows-active');
	});

	$('#phone-id').mask('+7(999) 999-99-99',{placeholder: "+7 (   )   -  -  "});

	//E-mail Ajax Send
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			$(location).attr('href', '/thanks.html');
		});
		return false;
	});

});

